import React, { Component } from 'react';


class ContactezNous extends Component{
    render(){
        return (
                <section className="contact">
                    <form action="" method="" className="formulairecontact">
                        <h3>Contactez-Nous</h3>
                        <table>
                            <tr>
                                <td>
                                    <tr>
                                        <input type="text" placeholder="Nom" 
                                            name="nom"/>
                                    </tr>
                                    <tr>
                                        <input type="email" placeholder="Adresse
                                            mail" name="adressemail"/>
                                    </tr>
                                    <tr>
                                        <input type="tel" placeholder="Téléphone" 
                                            name="telephone"/>
                                    </tr>
                                </td>
                                <td>
                                    <tr>
                                         <textarea placeholder="Message">

                                        </textarea>
                                    </tr>
                                    <tr>
                                        <input type="submit" value="Valider" 
                                            name="valider"/>
                                    </tr>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <nav>
                        <h3>Etices Dynamics</h3>
                        <ul>
                            <li>
                                <a href="http://localhost:3000/" title="facebook">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost:3000/" title="google">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost:3000/" title="Vkontakte">
                                    <i class="fab fa-vk"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost:3000/" title="YouTube">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost:3000/" title="LinkedIn">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost:3000/" title="DIGG">
                                    <i class="fab fa-digg"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://localhost:3000/" title="Deviantart">
                                    <i class="fab fa-deviantart"></i>
                                </a>
                            </li>
                        </ul> 
                    </nav>
                    <footer>
                        <a href="mailto:fadresse@gmail.com" title="email">
                            <figure>
                                <span>
                                    <i class="far fa-envelope"></i>
                                </span>
                                <figcaption>
                                    <span className="title"> EMAIL</span>
                                    <span className="description">
                                        fadresse@gmail.com
                                    </span>
                                </figcaption>
                            </figure>
                        </a>
                        <strong>
                            Designed by PSDFreebies.com
                        </strong>
                        <figure>
                            <i class="fas fa-mobile-alt"></i>
                            <figcaption>
                                <span className="title">CALL US</span>
                                <span className="description">
                                    <a href="tel:+22966666666" 
                                        title="Téléphone">
                                        +229 66 66 66 66
                                    </a>
                                    <a href="tel:+22966666666" 
                                        title="Téléphone">
                                        +229 66 66 66 66
                                    </a>
                                </span>
                            </figcaption>
                        </figure>
                    </footer>
                </section>
                
        )
    }
}
export default ContactezNous;