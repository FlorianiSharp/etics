import React, { Component } from 'react';


class ArticleComponent extends Component{
    render(){
        return (
                <article className="services_realisations_equipe">
                    <h3 className="htitle">Nos Services</h3>
                    <ul className="nosservices">
                        <li>
                            <figure>
                                <span className="siteetapplication">
                                </span>
                                <figcaption>
                                    <span className="title">
                                        SITES ET APPLICATION 
                                    </span>
                                    <span className="description">
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <figcaption>
                                    <span className="title"> 
                                        DESIGN GRAPHIQUE
                                    </span>
                                    <span className="description">
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                    </span>
                                </figcaption>
                                <span className="designgraphique">
                                </span>
                            </figure>
                        </li>
                    </ul>
                    <h3 className="htitle">Nos Réalisations</h3>
                    <ul className="nosrealisations">
                        <li>
                            <figure>
                                <span className="imgdesc einformatique"></span>
                                <figcaption>
                                    <span className="title"> 
                                        E-INFORMATIQUE
                                    </span>
                                    <span className="description">
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <span className="imgdesc quiz"></span>
                                <figcaption>
                                    <span className="title"> 
                                        QUIZ
                                    </span>
                                    <span className="description">
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <span className="imgdesc essupke"></span>
                                <figcaption>
                                    <span className="title"> 
                                        essupké_LANGUAGE_AND_ DIGITAL_INSTITUT
                                    </span>
                                    <span className="description">
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <span className="imgdesc kakushin"></span>
                                <figcaption>
                                    <span className="title"> 
                                        KAKUSHIN CORPORATION
                                    </span>
                                    <span className="description">
                                        Lorem, ipsum dolor sit amet consectetur 
                                        adipisicing elit. Accusantium doloremque 
                                        eum sit molestias ab unde ducimus 
                                        inventore magni ex cum, reiciendis illo, 
                                        aspernatur facere rerum tempore nobis nisi. 
                                        Inventore, tempora.
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                    </ul>
                    <h3 className="htitle">Notre Equipe</h3>
                    <ul className="notreequipe">
                        <li>
                            <h3>Graphiste et web designer</h3>
                            <figure>
                                <span className="imgdesc graphismeetdesign"></span>
                                <figcaption>
                                    Lorem, ipsum dolor sit amet consectetur 
                                    adipisicing elit. Accusantium doloremque 
                                    eum sit molestias ab unde ducimus 
                                    inventore magni ex cum, reiciendis illo, 
                                    aspernatur facere rerum tempore nobis nisi. 
                                    Inventore, tempora.
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <h3>Développeur d'application</h3>
                            <figure>
                                <span className="imgdesc developpeurapplication"></span>
                                <figcaption>
                                    Lorem, ipsum dolor sit amet consectetur 
                                    adipisicing elit. Accusantium doloremque 
                                    eum sit molestias ab unde ducimus 
                                    inventore magni ex cum, reiciendis illo, 
                                    aspernatur facere rerum tempore nobis nisi. 
                                    Inventore, tempora.
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <h3>Communication</h3>
                            <figure>
                                <span className="imgdesc communication"></span>
                                <figcaption>
                                    Lorem, ipsum dolor sit amet consectetur 
                                    adipisicing elit. Accusantium doloremque 
                                    eum sit molestias ab unde ducimus 
                                    inventore magni ex cum, reiciendis illo, 
                                    aspernatur facere rerum tempore nobis nisi. 
                                    Inventore, tempora.
                                </figcaption>
                            </figure>
                        </li>
                    </ul>
                    <p className="someparagraphe">
                        Lorem, ipsum dolor sit amet consectetur 
                        adipisicing elit. Accusantium doloremque 
                        eum sit molestias ab unde ducimus 
                        inventore magni ex cum, reiciendis illo, 
                        aspernatur facere rerum tempore nobis nisi. 
                        Inventore, tempora.
                        Lorem, ipsum dolor sit amet consectetur 
                        adipisicing elit. Accusantium doloremque 
                        eum sit molestias ab unde ducimus 
                        inventore magni ex cum, reiciendis illo, 
                        aspernatur facere rerum tempore nobis nisi. 
                        Inventore, tempora.
                    </p>
                </article>
        )
    }
}
export default ArticleComponent;