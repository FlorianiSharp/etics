import React, { Component } from 'react';
import Menus from './menus';
import LanderPage from "./landerpage"


class HeaderComponent extends Component{
    render(){
        return (
                <header>
                    <Menus />
                    <LanderPage />
                </header>
        )
    }
}
export default HeaderComponent;