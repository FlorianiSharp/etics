import React, { Component } from 'react';
import logo from "../../images/logo.png";

class Menus extends Component{
    render(){
        return (                
                <nav className="barredenavigation">
                    <figure>
                        <img src={logo} className="logo" alt="ETICES DYNAMICS" />
                    </figure>
                    <ul>
                        <a href="http://localhost:3000/" > 
                            <li>
                                Accueil
                            </li>
                        </a>
                        <a href="http://localhost:3000/" > 
                            <li>
                                Services
                            </li>
                        </a>
                        <a href="http://localhost:3000/" > 
                            <li>
                                Réalisations
                            </li>
                        </a>
                        <a href="http://localhost:3000/" > 
                            <li>
                                Equipes
                            </li>
                        </a>
                        <a href="http://localhost:3000/" > 
                            <li>
                                Contact
                            </li>
                        </a>
                    </ul>
                </nav>
        )
    }
}

export default Menus;