import React, { Component } from 'react';

class LanderPage extends Component{
    render(){
        return (
                <figure className="landerpage">
                    <figcaption>
                        <h1>
                            <span className="name"> ETICES DYNAMICS</span>
                            <span className="slogan">Let's dream and create</span>
                        </h1>
                    </figcaption>
                </figure>
        )
    }
}

export default LanderPage;