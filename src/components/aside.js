import React, { Component } from 'react';


class AsideComponent extends Component{
    render(){
        return (
                <aside className="atouts">
                    <ul>
                        <li>
                            <figure>
                                <span className="circle">
                                    <i class="fas fa-layer-group"></i>
                                </span>
                                <figcaption>
                                    <span className="title">dynamisme </span>
                                    <span className="description">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipscing elit, sed do eiusmod
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <span className="circle">
                                    <i class="far fa-sun"></i>
                                </span>
                                <figcaption>
                                    <span className="title">creativité </span>
                                    <span className="description">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipscing elit, sed do eiusmod
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <span className="circle">
                                    <i class="far fa-clock"></i>
                                </span>
                                <figcaption>
                                    <span className="title">rigueur </span>
                                    <span className="description">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipscing elit, sed do eiusmod
                                    </span>
                                </figcaption>
                            </figure>
                        </li>
                    </ul>
                </aside>
        )
    }
}
export default AsideComponent;