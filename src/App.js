import './App.css';
import './css/style.css';
import React, { Component } from "react";
import HeaderComponent from './components/headerComponents/header';
import AsideComponent from "./components/aside";
import ArticleComponent from "./components/article";
import ContactezNous from "./components/contact";
class App extends Component {
  render(){
    return (
      <div className="App">
          <HeaderComponent />
          <AsideComponent />
          <ArticleComponent />
          <ContactezNous />
      </div>
    )
  }
}

export default App;
